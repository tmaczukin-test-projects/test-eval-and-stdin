#!/usr/bin/env bash

set -eo pipefail
set +o noclobber

commands='echo -e "\e[32;1m./main\e[0m"; ./main; echo -e "\e[32;1mdate\e[0m"; date'

case "$1" in
  unwanted)
    eval "${commands}"
    ;;
  old)
    : | eval "${commands}"
    ;;
  new-v1)
    : | (eval "${commands}")
    ;;
  new-v2)
    exec 0<&-
    eval "${commands}"
    ;;
  *)
    echo "Use $0 unwanted|old|new-v1|new-v2"
    exit 1
esac

exit 0
