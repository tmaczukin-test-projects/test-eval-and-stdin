package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"time"
)

func main() {
	fmt.Printf("Starting process wtih pid=%d\n", os.Getpid())

	started := make(chan struct{})
	done := make(chan struct{})
	cancelFn := handleTimeoutAndTermination(started, done)

	<-started

	go func() {
		fmt.Println("Reading STDIN...")
		data, err := ioutil.ReadAll(os.Stdin)
		if err != nil {
			panic(fmt.Sprintf("Failed to read STDIN: %v", err))
		}

		fmt.Printf("Data read: %s\n", data)

		cancelFn()
	}()

	if len(os.Args) > 1 && os.Args[1] == "fail" {
		code := 123
		fmt.Printf("Fail requested. Exiting with code %d\n", code)
		os.Exit(code)
	}

	<-done
}

func handleTimeoutAndTermination(started chan struct{}, done chan struct{}) context.CancelFunc {
	signalCh := make(chan os.Signal)
	signal.Notify(signalCh, os.Interrupt)

	ctx, cancelFn := context.WithCancel(context.Background())

	go func() {
		defer close(done)

		fmt.Println("Starting timeout and signal handler")

		close(started)

		select {
		case <-ctx.Done():
		case <-time.After(5 * time.Second):
			fmt.Println("Timeout exceeded")
		case sig := <-signalCh:
			fmt.Printf("Signal %v received\n", sig)
			return
		}
	}()

	return cancelFn
}
